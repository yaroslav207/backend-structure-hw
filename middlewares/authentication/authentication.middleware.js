const jwt = require('jsonwebtoken');

const authentication = (req, res, next) => {
    try{
        let token = req.headers['authorization'];
        if (!token) {
            throw new Error()
        }
        token = token.replace('Bearer ', '');
        req.tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
        next();
    } catch (err) {
        return res.status(401).send({error: 'Not Authorized'});
    }
};
module.exports = {authentication};
