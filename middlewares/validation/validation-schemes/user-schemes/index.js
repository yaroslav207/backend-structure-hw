const {schema: getUserByIdSchema} = require('./getUserById.schemes');
const {schema: createUserSchema} = require('./createUser.schemes');
const {schema: editUserSchema} = require('./getUserById.schemes');

module.exports = {
    getUserByIdSchema,
    createUserSchema,
    editUserSchema
};