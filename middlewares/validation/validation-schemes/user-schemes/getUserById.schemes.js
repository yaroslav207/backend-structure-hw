const joi = require('joi');

const schema = joi.object({
    id: joi.string().uuid(),
}).required();

module.exports = {
    schema
};