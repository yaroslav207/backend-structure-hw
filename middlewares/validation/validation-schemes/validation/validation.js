const userSchemaValidation = require('../user-schemes');
const transactionSchemaValidation = require('../transactions-schemes');

function validation(schema) {
    return (data) => schema.validate(data);
}

const userValidation = {
    getUserByIdValidator: validation(userSchemaValidation.getUserByIdSchema),
    createUserValidator: validation(userSchemaValidation.createUserSchema),
    editUserValidator: validation(userSchemaValidation.editUserSchema),
};

const transactionsValidation = {
    createTransactionValidator: validation(transactionSchemaValidation.createTransactionSchema),
};

module.exports = {
    ...userValidation,
    ...transactionsValidation
};