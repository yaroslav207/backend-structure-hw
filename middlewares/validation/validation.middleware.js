const userValidationMiddleware = require('./user-validation-middleware/user.validation.middleware');
const transactionValidationMiddleware = require('./transaction-validation-middleware/transaction.validation.middleware');

module.exports = {
    ...userValidationMiddleware,
    ...transactionValidationMiddleware
};