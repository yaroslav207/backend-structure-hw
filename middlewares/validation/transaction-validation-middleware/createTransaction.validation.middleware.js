const { createTransactionValidator } = require('../validation-schemes/validation/validation');

const createTransactionValidation = (req, res, next) => {
    const isValidResult = createTransactionValidator(req.body);
    if(isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
    } else {
        next()
    }
};

module.exports = {
    createTransactionValidation
};