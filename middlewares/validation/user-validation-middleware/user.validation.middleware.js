const { createUserValidation } = require('./createUser.validation.middleware');
const { editUserValidation } = require('./editUser.validation.middleware');
const { getUserByIdValidation } = require('./getUserById.validation.middleware');

module.exports = {
    createUserValidation,
    editUserValidation,
    getUserByIdValidation
};