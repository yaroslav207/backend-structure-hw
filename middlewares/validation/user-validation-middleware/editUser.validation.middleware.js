const { editUserValidator } = require('../validation-schemes/validation/validation');

const editUserValidation = (req, res, next) => {
    const isValidResult = editUserValidator(req.body);
    if(isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
    } else {
        next()
    }
};

module.exports = {
    editUserValidation
};