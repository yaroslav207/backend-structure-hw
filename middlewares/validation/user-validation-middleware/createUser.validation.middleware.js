const { createUserValidator } = require('../validation-schemes/validation/validation');

const createUserValidation = (req, res, next) => {
    const isValidResult = createUserValidator(req.body);
    if(isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
    } else {
        next()
    }
};

module.exports = {
    createUserValidation
};