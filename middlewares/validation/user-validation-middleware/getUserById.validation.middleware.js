const { getUserByIdValidator } = require('../validation-schemes/validation/validation');

const getUserByIdValidation = (req, res, next) => {
    const isValidResult = getUserByIdValidator(req.params.id);
    if(isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
    } else {
        next()
    }
};

module.exports = {
    getUserByIdValidation
};