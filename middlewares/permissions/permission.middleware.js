
const permissionAdmin = (req, res, next) => {
    try{
        if(req.tokenPayload.type !== 'admin'){
            throw new Error()
        }
        next();
    } catch (err) {
        console.log('admin')
        return res.status(401).send({error: 'Not Authorized'});
    }
};
module.exports = {permissionAdmin};
