const validationMiddleware = require('./validation/validation.middleware');
const { authentication } = require('./authentication/authentication.middleware');
const { permissionAdmin } = require('./permissions/permission.middleware')

module.exports = {
    validationMiddleware,
    authentication,
    permissionAdmin
};

