
const { formatData, createJwt } = require('../../helpers/helpers');


class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const [user] = await this._userRepository.getUserById(id);

    return formatData(user);
  }

  async createUser(user) {
    const [newUser] = await this._userRepository.addUser({ ...user, balance: 0 });
    const accessToken = createJwt({id: newUser.id, type: newUser.type});
    const formatNewUser = formatData(newUser);

    return {...formatNewUser, accessToken};
  }

  async editUser(id, user) {
    const [editUser] = await this._userRepository.updateUser(id, {...user, updated_at: new Date()});

    return formatData(editUser);
  }
}

module.exports = { User };
