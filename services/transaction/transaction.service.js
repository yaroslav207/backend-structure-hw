const {formatData} = require('../../helpers/helpers');


class Transaction {
    constructor({transactionRepository}) {
        this._userRepository = transactionRepository;
    }

    async createTransaction(transaction) {
        transaction.card_number = transaction.cardNumber;
        delete transaction.cardNumber;
        transaction.user_id = transaction.userId;
        delete transaction.userId;

        const [newTransaction] = await this._userRepository.addTransaction(transaction);
        const formatNewTransaction = formatData(newTransaction);
        return formatNewTransaction;
    }
}

module.exports = {Transaction};
