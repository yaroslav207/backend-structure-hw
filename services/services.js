const {
    user: userRepository,
    transaction: transactionRepository
} = require('../data/repositories/repositories');

const {User} = require('./user/user.service');
const {Transaction} = require('./transaction/transaction.service');

const user = new User({
    userRepository
});

const transaction = new Transaction({
  transactionRepository
});

module.exports = {user, transaction};
