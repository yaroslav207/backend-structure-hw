const jwt = require("jsonwebtoken");

const createJwt = (data) => {
    return jwt.sign(data, process.env.JWT_SECRET);
};

module.exports = {
    createJwt
};

