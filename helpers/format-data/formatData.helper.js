const formatData = (obj) => {
    const formatObj = {...obj};

    Object.keys(formatObj).forEach(whatakey => {
        let index = whatakey.indexOf('_');
        if(index === -1){
            return
        }
        let newKey = whatakey.replace('_', '');
        newKey = newKey.split('')
        newKey[index] = newKey[index].toUpperCase();
        newKey = newKey.join('');
        formatObj[newKey] = formatObj[whatakey];
        delete formatObj[whatakey];
    });

    return formatObj;
};

module.exports = {
    formatData
};