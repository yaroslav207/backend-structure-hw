const formatUser = ({id, type, balance, name, email, phone, created_at: createdAt, updated_at: updatedAt}) => {
    return {
        id,
        type,
        balance,
        name,
        email,
        phone,
        createdAt,
        updatedAt
    }
};

module.exports = {
    formatUser
};