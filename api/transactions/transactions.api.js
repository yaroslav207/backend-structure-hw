const {validationMiddleware, authentication, permissionAdmin} = require('../../middlewares/middlewares');

const {
    createTransactionValidation
} = validationMiddleware;

const initTransaction = (Router, services) => {
    const {
        transaction: transactionService,
        user: userService,
    } = services;

    const router = Router();

    router.post("/",
        [
            createTransactionValidation,
            authentication,
            permissionAdmin
        ],
        async (req, res) => {
            const user = await userService.getUserById(req.body.userId);
            console.log(user);
            if (!user) {
                res.status(400).send({error: 'User does not exist'});
                return;
            }
            transactionService.createTransaction(req.body).then(result => {
                const currentBalance = req.body.amount + user.balance;
                userService.editUser(req.body.user_id, {balance: currentBalance})
                    .then(result => {
                        return res.send({
                            ...result
                        });
                    });
            }).catch(err => {
                console.log(err);
                res.status(500).send("Internal Server Error");
                return;
            });
        });
    return router
};

module.exports = {
    initTransaction
};