const { initUser } = require('./users/user.api');
const { initTransaction } = require('./transactions/transactions.api');
const { user, transaction } = require('../services/services');


const initApi = Router => {
    const apiRouter = Router();

    apiRouter.use(
        '/users',
        initUser(Router, {
            user
        })
    );

    apiRouter.use(
        '/transactions',
        initTransaction(Router, {
            user,
            transaction
        })
    );

    return apiRouter;
};

module.exports = {
    initApi
};