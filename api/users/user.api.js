const {validationMiddleware, authentication} = require('../../middlewares/middlewares');
const ee = require('events');

const statEmitter = new ee();
const stats = {
    totalUsers: 3,
    totalBets: 2,
    totalEvents: 3,
};

const {
    getUserByIdValidation,
    createUserValidation,
    editUserValidation
} = validationMiddleware;

const initUser = (Router, services) => {
    const {user: userService} = services;
    const router = Router();


    router.get("/:id", getUserByIdValidation, (req, res) => {
        try {
            userService.getUserById(req.params.id)
                .then(result => {
                    if (!result) {
                        res.status(404).send({error: 'User not found'});
                    }
                    return res.status(200).send({
                        ...result,
                    });
                });
        } catch (err) {
            console.log(err);
            res.status(500).send("Internal Server Error");
        }
    })
        .post("/", createUserValidation, (req, res) => {
            userService.createUser(req.body)
                .then(result => {
                    statEmitter.emit('newUser');
                    return res.send(result);
                }).catch(err => {
                if (err.code == '23505') {
                    res.status(400).send({
                        error: err.detail
                    });
                }
                console.log(err);
                res.status(500).send("Internal Server Error");
            });
        })
        .put("/:id",
            [
                editUserValidation,
                authentication
            ],
            (req, res) => {
            if (req.params.id !== tokenPayload.id) {
                return res.status(401).send({error: 'UserId mismatch'});
            }
            userService.editUser(req.params.id, req.body)
                .then(result => {
                    res.status(200).send({...result});
                })
                .catch(err => {
                console.log(err);
                res.status(500).send("Internal Server Error");
            });
        });

    return router
};

module.exports = {
    initUser
};