const {Abstract} = require('../abstract/abstract.repository');

class User extends Abstract {
    constructor(model) {
        super(model);
    }

    addUser(user) {
        return this
            .create(user)
            .returning('*');
    }

    getUserById(id) {
        return this
            .getById(id)
            .returning('*');
    }

    updateUser(id, data) {
        return this
            .updateById(id, data)
            .returning('*');
    }
}

module.exports = {
    User
};