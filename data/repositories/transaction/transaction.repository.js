const {Abstract} = require('../abstract/abstract.repository');

class Transaction extends Abstract {
    constructor(model) {
        super(model);
    }

    addTransaction(transaction) {
        return this
            .create(transaction)
            .returning('*');
    }
}

module.exports = {
    Transaction
};