class Abstract {
    constructor(model) {
        this.model = model;
    }

    getById(id) {
        return this.model.where('id', id);
    }

    create(data){
        return this.model.insert(data);
    }

    updateById(id, data) {
        return this.model.where('id', id).update(data);
    }
}

module.exports = { Abstract };