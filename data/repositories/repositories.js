const { db } = require('../db/connect/connect');

const userModel = db('user');
const transactionModel = db('transaction');
const eventModel = db('event');
const betModel = db('bet');

const { User } = require('./user/user.repository');
const { Transaction } = require('./transaction/transaction.repository');

const user = new User(userModel);
const transaction = new Transaction(transactionModel);


module.exports = { user, transaction };
