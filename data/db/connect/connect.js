const knex = require("knex");
const dbConfig = require("../../../knexfile");

const db = knex(dbConfig.development);

db.raw('select 1+1 as result')
    .then(() => {
        console.log('db connected')
    })
    .catch(() => {
        throw new Error('No db connection');
    });

module.exports = {
    db
};
